//
//  main.m
//  NetworkFrameworkTest
//
//  Created by front on 16/3/18.
//  Copyright © 2016年 FelixHub. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
