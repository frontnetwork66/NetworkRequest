//
//  AppDelegate.h
//  NetworkFrameworkTest
//
//  Created by front on 16/3/18.
//  Copyright © 2016年 FelixHub. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

